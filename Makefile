# Makefile for echoserver and echoclient
# Jian Lik Ng

# Compiler flags
CC = g++
CFLAGS = -Wall -g
LDFLAGS += -lpthread

# Targets
all: runEchoServer runEchoClients

runEchoServer: runEchoServer.o echoServer.o
	$(CC) -o $@ $(CFLAGS) $^ $(LDFLAGS)

echoServer: echoServer.o
	$(CC) -o $@ $(CFLAGS) $^ $(LDFLAGS)

runEchoClients: runEchoClients.o echoClient.o
	$(CC) -o $@ $(CFLAGS) $^ $(LDFLAGS)

echoClient: echoClient.o
	$(CC) -o $@ $(CFLAGS) $^ $(LDFLAGS)

%.o : %.cc
	$(CC) -c -o $@ $(CFLAGS) $(ASAN_FLAGS) $<

.PHONY: clean

clean:
	rm -rf *.o echoServer runEchoServer echoClient runEchoClients