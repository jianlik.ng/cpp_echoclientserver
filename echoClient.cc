/* echoClient.cc 
 * 
 * Author         Comments            Date
 * --------       --------            ------
 * Jian Lik Ng    Initial release     Oct. 29/22
 * 
 * This class implements a simple echoClient
 * 
 */

// Includes

// User includes
#include "echoDef.hh"
#include "echoClient.hh"

/*******************************************************************************
 * Function:    Constructor
 *
 * Inputs:      None
 * 
 * Outputs:     None
 * 
 * Returns:     None
 * 
 * Description: Sets the port and number of threads. 
 *              Initializes threads and synchronization variables.
 * 
 ******************************************************************************/
echoClient::echoClient() : 
  server_{ defaultServer },
  port_{ defaultPort }, 
  delay_{ 1 },
  clientId_{ 0 },
  nEchoes_ { 3 },
  initialized_ { false }
{
  // Nop
}

/*******************************************************************************
 * Function:    Constructor
 *
 * Inputs:      None
 * 
 * Outputs:     None
 * 
 * Returns:     None
 * 
 * Description: Sets the port and number of threads. 
 *              Initializes threads and synchronization variables.
 * 
 ******************************************************************************/
echoClient::echoClient(
  const char* server,
  int port,
  int delay,
  int clientId,
  int nEchoes)
{
  server_ = server;
  port_ = port;
  delay_ = delay;
  clientId_ = clientId;
  nEchoes_ = nEchoes;
  initialized_ = false;
}

/*******************************************************************************
 * Function:    Destructor
 *
 * Inputs:      None
 * 
 * Outputs:     None
 * 
 * Returns:     None
 * 
 * Description: Cleans up variables
 * 
 ******************************************************************************/
echoClient::~echoClient()
{
}

/*******************************************************************************
 * Function:    init
 *
 * Inputs:      None
 * 
 * Outputs:     None
 * 
 * Returns:     resCode:      Error code
 * 
 * Description: Initializes client with message
 * 
 ******************************************************************************/
void echoClient::init(const char* msg)
{
  strncpy(msg_, msg, msglen);

  initialized_ = true;
}

/*******************************************************************************
 * Function:    openClientSocket
 *
 * Inputs:      None
 * 
 * Outputs:     None
 * 
 * Returns:     resCode:      Error code
 * 
 * Description: Opens client socket
 * 
 ******************************************************************************/
int 
echoClient::openClientSocket()
{
  // Get variables
  char portStr[sizeof(int)*4];    // Convert portno to a string
  snprintf(portStr, sizeof(int)*4, "%d", port_);

  // Socket implementation
  struct addrinfo  hints;           // Contains socket parameters
  struct addrinfo* servInfo;        // Found connections
  memset(&hints, 0, sizeof(hints)); // Clear data
  hints.ai_family = AF_UNSPEC;      // Don't care IPv4 or IPv6
  hints.ai_socktype = SOCK_STREAM;  // TCP stream sockets

  int res = getaddrinfo( // If non-zero return, there is an error
      server_,            // Http address (if NULL and AI_PASSIVE, then accept 
                         //   connections on any of the hosts' network 
                         //   addresses)
      portStr,           // Port (if NULL, then the port of the returned socket
                         //   addresses will be left uninitialized)
      &hints,            // Socket parameters
      &servInfo);        // Linked list of addresses that are found

  if(res != 0)
  {
    std::cout << "getaddrinfo failed!" << std::endl;
    freeaddrinfo(servInfo);
    return -1;
  }

  // Open socket
  sfd_ = socket(servInfo->ai_family, 
                servInfo->ai_socktype, 
                servInfo->ai_protocol);
  
  // Connect to found socket
  while(connect(sfd_, servInfo->ai_addr, servInfo->ai_addrlen) == -1)
  {
    std::cout << "Could not connect to socket... trying again" << std::endl;
    sleep(1);
  }

  freeaddrinfo(servInfo); // No longer need servInfo

  std::cout << "Connected to server!" << std::endl;

  return 0;
}

/*******************************************************************************
 * Function:    echo
 *
 * Inputs:      None
 * 
 * Outputs:     None
 * 
 * Returns:     resCode:      Error code
 * 
 * Description: Sends multiple echoes.
 * 
 ******************************************************************************/
int echoClient::echo()
{
  if(!initialized_)
  {
    std::cout << "client " << clientId_ << " not initialized!" << std::endl;
    return -1;
  }

  if(openClientSocket() < 0)
  {
    std::cout << "open client socket failed!" << std::endl;
    return -1;
  }

  char tempBuffer[msglen];

  for(int i = 0; i < nEchoes_; i++)
  {
    size_t sentBytes = send(sfd_, msg_, msglen, 0);
    if(sentBytes <= 0)
    {
      std::cout << "send failed: " << strerror(errno) << std::endl;
      return -1;
    }

    std::cout << "Client " << clientId_ << " sent: " << msg_ << std::endl;

    size_t recvBytes = recv(sfd_, tempBuffer, msglen, 0);
    if(recvBytes <= 0)
    {
      std::cout << "recv failed: " << strerror(errno) << std::endl;
      return -1;
    }

    std::cout << "Client " << clientId_ << " received: " 
      << tempBuffer << std::endl;

    sleep(delay_);
  }

  close(sfd_);

  return 0;
}