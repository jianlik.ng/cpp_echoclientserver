#ifndef __ECHOCLIENT_HH__
#define __ECHOCLIENT_HH__

/* echoClient.cc 
 * 
 * Author         Comments            Date
 * --------       --------            ------
 * Jian Lik Ng    Initial release     Oct. 29/22
 * 
 * This class implements a simple echoClient
 * 
 */

// Includes

// Echo server class
class echoClient{

  public: 
  echoClient();                     // Default Constructor
  echoClient(                       // Constructor  
    const char* server,
    int port,
    int delay,
    int clientId,
    int nEchoes);

  virtual ~echoClient();            // Destructor (virtual so derived classes
                                    // properly destroyed)
  // Public functions 
  int echo(void);                   // perform echo
  void init(const char*);            // set message       

  // Other setter/getter functions...

  // Public variables

  protected:

  private:

  // Private functions
  int openClientSocket();           // opens client socket

  // Private variables
  const char* server_;             // server
  int port_;                        // port number
  int sfd_;                         // client socket file descriptor
  int delay_;                       // echo delay
  int clientId_;                    // client id
  int nEchoes_;                     // number of echoes to send
  bool initialized_;                // initialized status
  char msg_[msglen];                // message

};


#endif