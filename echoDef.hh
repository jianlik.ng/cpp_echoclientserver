#ifndef __ECHO_DEF_HH__
#define __ECHO_DEF_HH__

// Common includes, defines

// C
#include <netinet/in.h>
#include <netdb.h>
#include <sys/socket.h>
#include <errno.h>
#include <string.h>
#include <functional>
#include <unistd.h>
#include <getopt.h>

// C++
#include <iostream>
#include <string>
#include <vector>
#include <queue>
#include <thread>
#include <functional>

static const int msglen = 256;
static const int defaultPort = 10500;
static const char* defaultServer = "localhost";

#endif