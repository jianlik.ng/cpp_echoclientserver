/* echoServer.cc 
 * 
 * Author         Comments            Date
 * --------       --------            ------
 * Jian Lik Ng    Initial release     Oct. 29/22
 * 
 * This class implements a multithreaded echoServer
 * 
 */

// Includes

// User includes
#include "echoDef.hh"
#include "echoServer.hh"

std::function<void()> threadFunc(
    pthread_mutex_t* mutex_, 
    pthread_cond_t* reqPending_,
    std::queue<qObject*>* reqQ_);         // worker thread function

/*******************************************************************************
 * Function:    Constructor
 *
 * Inputs:      None
 * 
 * Outputs:     None
 * 
 * Returns:     None
 * 
 * Description: Sets up default variables
 * 
 ******************************************************************************/
echoServer::echoServer() : 
  port_{ defaultPort }, 
  nThreads_{ defaultThreads },
  initialized_{ false }
{
  // Nop
}

/*******************************************************************************
 * Function:    Constructor
 *
 * Inputs:      None
 * 
 * Outputs:     None
 * 
 * Returns:     None
 * 
 * Description: Sets up private variables
 * 
 ******************************************************************************/
echoServer::echoServer(
  int port, 
  int nThreads)
{
  port_ = port;
  nThreads_ = nThreads;
  initialized_ = false;
}


/*******************************************************************************
 * Function:    Destructor
 *
 * Inputs:      None
 * 
 * Outputs:     None
 * 
 * Returns:     None
 * 
 * Description: Cleans up variables
 * 
 ******************************************************************************/
echoServer::~echoServer()
{
  if(initialized_)
  {
    pthread_mutex_destroy(&mutex_);
    pthread_cond_destroy(&reqPending_);
  }

  threads_.clear();
}

/*******************************************************************************
 * Function:    init
 *
 * Inputs:      port:         port to communicate over
 *              nThreads_:     number of threads to start up
 * 
 * Outputs:     None
 * 
 * Returns:     resCode:      error code
 * 
 * Description: Sets the port and number of threads. 
 *              Initializes threads and synchronization variables.
 * 
 ******************************************************************************/ 
int echoServer::init()
{
  if(!initialized_)
  {
    // Initialize mutex
    if(pthread_mutex_init(&mutex_, NULL) <0)
    {
      std::cout << "mutex init failed" << std::endl;
      return -1;
    }

    // Initialize condition variable
    if(pthread_cond_init(&reqPending_, NULL) < 0)
    {
      std::cout << "cond var init failed" << std::endl;
      return -1;
    }
  }

  initialized_ = true;

  return 0;
}

/*******************************************************************************
 * Function:    serve
 *
 * Inputs:      None
 * 
 * Outputs:     None
 * 
 * Returns:     resCode:    error code
 * 
 * Description: Starts boss thread and worker threads, loops forever
 * 
 ******************************************************************************/ 
int echoServer::serve()
{
  if(!initialized_)
  {
    std::cout << "server not initialized" << std::endl;
    return -1;
  }

  // Open listening socket
  if(openListeningSocket() < 0)
  {
    std::cout << "open listening socket failed" << std::endl;
    return -1;
  }

  std::cout << "creating thread pool of " << nThreads_ << 
    " threads..." << std::endl;

  // Create pool of threads
  for(int i = 0 ; i < nThreads_; i++)
  {
    threads_.push_back(std::thread(threadFunc, &mutex_, &reqPending_, &reqQ_));
  }

  // Loop forever
  for(;;)
  {
    // Sockets
    int clientSfd;
    if(acceptClientConnection(
      &clientSfd) < 0)
    {
      std::cout << "accept client socket failed" << std::endl;
      return -1;
    }

    std::cout << "Client accepted!" << std::endl;

    // Create new request
    qObject* newReq = new qObject;
    newReq->sfd = clientSfd;

    std::cout << "New request enqueued!" << std::endl;

    // Enqueue request
    pthread_mutex_lock(&mutex_); // Lock mutex
    reqQ_.push(newReq); // Add item to queue
    pthread_mutex_unlock(&mutex_); // Unlock mutex
    pthread_cond_signal(&reqPending_); // Signal condition variable

  }

}

/*******************************************************************************
 * Function:    threadFunc
 *
 * Inputs:      None
 * 
 * Outputs:     None
 * 
 * Returns:     None
 * 
 * Description: Server worker thread function
 * 
 ******************************************************************************/
std::function<void()> threadFunc(
    pthread_mutex_t* mutex_, 
    pthread_cond_t* reqPending_,
    std::queue<qObject*>* reqQ_)
{

  std::cout << "Worker thread started!" << std::endl;

  for(;;)
  {
    // --- Critical section ----------------------------------------------------
    pthread_mutex_lock(mutex_);    // Lock mutex
    while(reqQ_->empty())
    {
      pthread_cond_wait(reqPending_, mutex_); 
      // Wait on condition variable (release mutex if condition not true)
    }

    // Dequeue a file request from the request queue
    qObject* req = reqQ_->front();
    reqQ_->pop();
    pthread_mutex_unlock(mutex_);
    // -------------------------------------------------------------------------

    // Echo
    char tempBuffer[msglen];
    for(;;)
    {
      size_t recvBytes = recv(req->sfd, tempBuffer, msglen, 0);
      if(recvBytes == 0)
      {
        break; // Client closed connection
      }
      else if (recvBytes < 0)
      {
        std::cout << "recv failed: " << strerror(errno) << std::endl;
        break;
      }
      size_t sentBytes = send(req->sfd, tempBuffer, msglen, 0);
      if(sentBytes < 0)
      {
        std::cout << "send failed: " << strerror(errno) << std::endl;
        break;   
      }

    }

    // Delete request object
    delete(req);
    req = NULL;
  }
}


/*******************************************************************************
 * Function:    openListeningSocket
 *
 * Inputs:      None
 * 
 * Outputs:     None
 * 
 * Returns:     resCode:    error code
 * 
 * Description: Opens listening socket
 * 
 ******************************************************************************/ 
int echoServer::openListeningSocket()
{
  // This is adapted from Beej's guide to socket programming
  // https://beej.us/guide/bgnet/html/

  // Create socket connection
  struct addrinfo hints;              // Contains socket parameters
  struct addrinfo* servInfo;          // Found connections
  memset(&hints, 0, sizeof(hints));   // Clear data
  hints.ai_family = AF_UNSPEC;        // don't care IPv4 or IPv6
  hints.ai_socktype = SOCK_STREAM;    // TCP stream sockets
  hints.ai_flags = AI_PASSIVE;        // Fill in my IP for me

  char portStr[sizeof(int)*4]; 
  snprintf(portStr, sizeof(int)*4, "%d", port_);

  int res = getaddrinfo( // If non-zero return, there is an error
        NULL,            // Http address (if NULL and AI_PASSIVE, then accept 
                         //   connections on any of the hosts' addresses)
        portStr,         // Port (if NULL, then the port of the returned socket
                         //   addresses will be left uninitialized)
        &hints,          // Socket parameters
        &servInfo);      // Linked list of addresses that are found

  if(res != 0)
  {
    std::cout << "getaddrinfo failed: " << strerror(errno) << std::endl;
    freeaddrinfo(servInfo);
    return -1;
  }
  
  // Open listener socket
  listenerSfd_ = socket(servInfo->ai_family, 
                        servInfo->ai_socktype, 
                        servInfo->ai_protocol);

  // Unbind socket
  int yes = 1;
  if (setsockopt(listenerSfd_, SOL_SOCKET, SO_REUSEADDR, &yes, sizeof yes) == -1) 
  {
    std::cout << "setsockopt failed: " << strerror(errno) << std::endl;
    freeaddrinfo(servInfo);
    return -1;
  } 

  // Servers need to bind the socket so server runs on a specified port
  // Don't always have to bind - eg. for clients you might not care what port
  // you are on, you only care about the remote server port
  if(bind(listenerSfd_, servInfo->ai_addr, servInfo->ai_addrlen) == -1)
  {
    std::cout << "bind failed: " << strerror(errno) << std::endl;
    freeaddrinfo(servInfo);
    return -1;
  } 

  freeaddrinfo(servInfo); // No longer need servInfo

  // Listen for connections
  int backlog = 100; // Number of requests queued 
  if(listen(listenerSfd_, backlog) == -1)
  {
    std::cout << "listen failed: " << strerror(errno) << std::endl;
    return -1;
  }

  return 0;
}

/*******************************************************************************
 * Function:    acceptClientConnection
 *
 * Inputs:      None
 * 
 * Outputs:     sfd:        client socket file descriptor
 * 
 * Returns:     resCode:    error code
 * 
 * Description: Opens listening socket
 * 
 ******************************************************************************/
int echoServer::acceptClientConnection(
  int*  sfd)
{
  socklen_t addrlen;
  struct sockaddr_storage clientAddr; //big enough to hold IPV4 or IPV6
  addrlen = sizeof(clientAddr);

  // Create the connection context
  if((*sfd = accept(listenerSfd_, 
              (struct sockaddr*)&clientAddr, &addrlen)) == -1)
  {
    printf("\nClient socket accept ERROR: %s\n", strerror(errno));
    return -1;
  }

  printf("\nConnection accepted ~");     

  return 0;
}