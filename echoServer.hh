#ifndef __ECHOSERVER_HH__
#define __ECHOSERVER_HH__

/* echoServer.hh 
 * 
 * Author         Comments            Date
 * --------       --------            ------
 * Jian Lik Ng    Initial release     Oct. 29/22
 * 
 * This class implements a multithreaded echoServer
 * 
 */

// Includes

// Constants, enums
const int defaultThreads = 25;

// Queue object
typedef struct qObject{
  int sfd;                          // client socket file descriptor
} qObject;

// Echo server class
class echoServer{

  public: 
  
  // Public functions
  echoServer();                     // Default Constructor
  echoServer(                       // Constructor  
    int port,       
    int nThreads);

  virtual ~echoServer();            // Destructor (virtual so derived classes
                                    // properly destroyed)

  int init();                       // intialization
   
  int serve(void);                  // starts server

  // Public variables

  protected:

  private:

  // Private functions
  int openListeningSocket();        // opens listening socket
  int acceptClientConnection(
    int* sfd);     // accepts client connection

  // Private variables
  int port_;                         // port number
  int nThreads_;                     // number of threads
  int listenerSfd_;                  // listener socket
  bool initialized_;                 // server initialized
  pthread_mutex_t mutex_;            // mutex to protect queue
  pthread_cond_t reqPending_;        // condition variable
  std::queue<qObject*> reqQ_;        // queue of socket file descriptors
  std::vector<std::thread> threads_; // threads

};

#endif