Jian Lik Ng
Oct. 29/22

# echoClient and echoServer

This is a sample echoServer and echoClient written in C/C++.
Feel free to reference this code for your own projects, but please cite where you got the code from.

## Directions

1. Run make
2. Start the server by running ./runEchoServer
3. Start the clients by running ./runEchoClients

Output should be printed to stdout.