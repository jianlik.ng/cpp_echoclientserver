/* runEchoClient.cc 
 * 
 * Author         Comments            Date
 * --------       --------            ------
 * Jian Lik Ng    Initial release     Oct. 29/22
 * 
 * Driver for several echoClients
 * 
 */

// Includes

// User includes
#include "echoDef.hh"
#include "echoClient.hh"


//OPTIONS
static struct option gLongOptions[] = {
  {"port",         required_argument,     NULL,        'p'},
  {"msg",          required_argument,     NULL,        'm'},
  {"nClients",     required_argument,     NULL,        'n'},
  {"server",       required_argument,     NULL,        's'},
  {NULL,           0,                     NULL,          0}
};


// Prompt to help user
void Usage()
{
  std::cout << "Usage: echoServer -p <port> -m <message> -n <nClients> "
    "-s <server>" << std::endl;
}

// Main entry point
int main(int argc, char **argv) {

  char option_char;
  char msg[msglen];
  char server[msglen];
  int port = defaultPort;
  int nClients = 5;
  strncpy(msg, "Hello", msglen);
  strncpy(server, defaultServer, msglen);

  while ((option_char = getopt_long(argc, argv, "p:m:n:s:", gLongOptions, NULL)) != -1) {
    switch (option_char) {
      default:
        Usage();
        exit(1);
      case 'h': // help
        Usage();
        exit(0);
        break;  
      case 'm': // number of threads
        if(strlen(optarg) > msglen)
        {
          std::cout << "Message too long! Max: " << msglen << std::endl;
          exit(1);
        }
        strncpy(msg, optarg, msglen);
        break;	
      case 'n': // clients
        nClients = atoi(optarg);
        break;
      case 'p': // port
        port = atoi(optarg);
        break;
      case 's': // port
        strncpy(server, optarg, msglen);
        break;
    }
  }

  int delay = 1;
  int nEchoes = 2;

  std::cout << "Server: " << server << std::endl;
  std::cout << "Port: " << port << std::endl;
  std::cout << "numClients: " << nClients << std::endl;
  std::cout << "Delay: " << delay << std::endl;
  std::cout << "numEchoes: " << nEchoes << std::endl;

  std::vector<echoClient> clients;
  
  for(int i = 0; i < nClients; i++)
  {
    std::string tmp;
    tmp.append(msg);
    tmp.append(std::to_string(i));
    clients.push_back(echoClient(server, port, delay, i, nEchoes));
    clients[i].init(tmp.c_str());
    if(clients[i].echo() < 0)
    {
      std::cout << "Error in client!" << std::endl;
    }
  }          

  return 0;

}

