/* runEchoServer.cc 
 * 
 * Author         Comments            Date
 * --------       --------            ------
 * Jian Lik Ng    Initial release     Oct. 29/22
 * 
 * Driver for echoServer
 * 
 */

// Includes

// User includes
#include "echoDef.hh"
#include "echoServer.hh"


//OPTIONS
static struct option gLongOptions[] = {
  {"port",         required_argument,     NULL,        'p'},
  {"nthreads",     required_argument,     NULL,        't'},
  {NULL,           0,                     NULL,          0}
};


// Prompt to help user
void Usage()
{
  std::cout << "Usage: echoServer -p <port> -t <nThreads>" << std::endl;
}

// Main entry point
int main(int argc, char **argv) {

  char option_char;
  int nthreads = 10;
  int port = defaultPort;

  while ((option_char = getopt_long(argc, argv, "p:t:", gLongOptions, NULL)) != -1) {
    switch (option_char) {
      default:
        Usage();
        exit(1);
      case 'h': // help
        Usage();
        exit(0);
        break;  
      case 't': // number of threads
        nthreads = atoi(optarg);
        break;				
      case 'p': // port
        port = atoi(optarg);
        break;
    }
  }

  echoServer server(port, nthreads); // Create server
  server.init(); // Initialize server

  std::cout << "Server initialized and listening on port: " << port 
    << std::endl;

  if(server.serve() < 0) // Start server
  {
    std::cout << "Error in server!" << std::endl;
  }              

  // Should never get here
  return -1;

}

